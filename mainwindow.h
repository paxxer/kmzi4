#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QLineEdit>
#include "originstat.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  QString numberMaker(QString);
  QString *origin;
  QString *modif;
  OriginStat *ors;
  OriginStat *mod;
  double correlation(QString *orig, QString *modif);
private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
