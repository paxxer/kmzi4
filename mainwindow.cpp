#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QByteArray>
#include <QMessageBox>
#include <bitset>


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ui->lineEdit->setReadOnly(true);
  ui->lineEdit_2->setReadOnly(true);
  this->setFixedSize(652, 277);
  this->setWindowTitle("КМЗИ 4");

  //ui->pushButton_5->hide();

  QObject::connect(ui->pushButton, &QPushButton::clicked, [=](){
      ui->lineEdit->setText(QFileDialog::getOpenFileUrl(NULL, "Открыть исходный файл", QUrl("./"), "Bitmap (*.bmp)").toString(QUrl::PreferLocalFile));
      origin = new QString(numberMaker(ui->lineEdit->text()));
    });

  QObject::connect(ui->pushButton_2, &QPushButton::clicked, [=](){
      ui->lineEdit_2->setText(QFileDialog::getOpenFileUrl(NULL, "Открыть преобразованный файл", QUrl("./"), "Bitmap (*.bmp)").toString(QUrl::PreferLocalFile));
      modif = new QString(numberMaker(ui->lineEdit_2->text()));
    });
  QObject::connect(ui->pushButton_3, &QPushButton::clicked, [=](){
      if(ui->lineEdit->text().isEmpty())
        QMessageBox::critical(this, "Ошибка", "Не выбран файл.", QMessageBox::Ok, QMessageBox::NoButton);
      else {
          ors = new OriginStat(origin);
          ors->exec();
        }
    });

  QObject::connect(ui->pushButton_4, &QPushButton::clicked, [=](){
      if(ui->lineEdit_2->text().isEmpty())
        QMessageBox::critical(this, "Ошибка", "Не выбран файл.", QMessageBox::Ok, QMessageBox::NoButton);
      else {
          mod = new OriginStat(modif);
          mod->exec();
        }
    });

  QObject::connect(ui->pushButton_5, &QPushButton::clicked, [=](){
      if(ui->lineEdit->text().isEmpty() || ui->lineEdit_2->text().isEmpty())
        QMessageBox::critical(this, "Ошибка", "Не выбраны файлы.", QMessageBox::Ok, QMessageBox::NoButton);
      else{
          double res = correlation(origin, modif);
          if(res != -1)
            QMessageBox::information(this, "Коэффициент взаимной корреляции", QString::number(res, 'f'),QMessageBox::Ok);
          else
            QMessageBox::critical(this, "Ошибка", "Изображения имеют разный размер", QMessageBox::Ok, QMessageBox::NoButton);
        }
    });

}

MainWindow::~MainWindow()
{
  delete ui;
}

QString MainWindow::numberMaker(QString path)
{
  QFile *file = new QFile(path);
  file->open(QIODevice::ReadOnly);
  QString str = "";
  QByteArray *ba = new QByteArray(file->readAll());

  for (int i = 0; i < ba->size(); ++i) {
      str += QString::fromStdString(std::bitset<8>(ba->at(i)).to_string());
    }
  //qDebug() << str;
  return str;
}

double MainWindow::correlation(QString *orig, QString *modif)
{
  typedef QString::size_type ulong;
  ulong matches, unmatches;
  matches = unmatches = 0;
  if(orig->size() != modif->size())
    return -1;
  else {
      for(ulong i = 448; i < orig->size(); ++i)
        if(orig->at(i) == modif->at(i))
          matches++;
        else
          unmatches++;
    }
  return (double)(matches - unmatches) / orig->size();
}
