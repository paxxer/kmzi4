#include "originstat.h"
#include "ui_originstat.h"
#include <QTextStream>

OriginStat::OriginStat(QString *file, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::OriginStat)
{
  ui->setupUi(this); this->setWindowTitle("Статистика исходного файла");
  ui->tableWidget->setColumnCount(10);
  ui->tableWidget->setRowCount(32);
  ui->tableWidget->verticalHeader()->hide();
  hstrl = new QStringList(QList<QString>({"1 бит", "Вероятность", "2 бита", "Вероятность", "3 бита", "Вероятность", "4 бита", "Вероятность", "5 бит", "Вероятность"}));
  ui->tableWidget->setHorizontalHeaderLabels(*hstrl);

  QObject::connect(ui->pushButton, &QPushButton::clicked, [=](){
      QString fr= QFileDialog::getSaveFileName(this,tr("Save"),"./",tr("(*.txt)"));
      QFile file(fr);
      file.open(QIODevice::WriteOnly | QIODevice::Text);
      int i;
      QTextStream out(&file);
      for (i=0;i<ui->tableWidget->rowCount();i++){
          for (int j=0;j<ui->tableWidget->columnCount(); j= j+2){
              if(ui->tableWidget->item(i,j) == 0x00)
                out << "\t";
              else
                {
                  out << ui->tableWidget->item(i,j)->text()+" ";
                  out << ui->tableWidget->item(i,j+1)->text();
                }
              out << "\t";
            }
          out<<"\n";
        };
      file.close();
    });


  for(int column = 0; column < 5; column++)
    for(int row = 0; row < 1 << (column + 1); row++)
      ui->tableWidget->setItem(row, (column * 2),
                               new QTableWidgetItem(QString("%1").arg(row, column+1, 2, QChar('0'))));
  fillTable(file);



}

OriginStat::~OriginStat()
{
  delete ui;
}

void OriginStat::fillTable(QString *file)
{
  QMap<QString, double> map;

  for(int column = 0; column < 5; column++)
    {
      quint32 tmp = 0;
      map.clear();
      for(int row = 0; row < 1 << (column + 1); row++)
        {
          map.insert(ui->tableWidget->item(row, (column * 2))->text(),
                     file->count(ui->tableWidget->item(row, (column * 2))->text()));
          tmp += map.last();
        }


      for(int row = 0; row < 1 << (column + 1); row++)
        {
          ui->tableWidget->setItem(row, (column * 2) + 1,
                                   new QTableWidgetItem(QString::number(map.values().at(row)/tmp, 'f')));
        }
    }
}
