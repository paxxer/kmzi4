#ifndef ORIGINSTAT_H
#define ORIGINSTAT_H

#include <QDialog>
#include <QFileDialog>
#include <QFile>


namespace Ui {
  class OriginStat;
}

class OriginStat : public QDialog
{
  Q_OBJECT

public:
  explicit OriginStat(QString *file, QWidget *parent = 0);
  ~OriginStat();
  QStringList *hstrl;
  void fillTable(QString *);

private:
  Ui::OriginStat *ui;
};

#endif // ORIGINSTAT_H
