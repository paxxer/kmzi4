#-------------------------------------------------
#
# Project created by QtCreator 2015-03-21T16:25:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kmzi4
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    originstat.cpp

HEADERS  += mainwindow.h \
    originstat.h

FORMS    += mainwindow.ui \
    originstat.ui

CONFIG += c++11
